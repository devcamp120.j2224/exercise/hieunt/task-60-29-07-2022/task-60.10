package com.devcamp.j60.j60jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.j60jpa.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}
